# Maintainer: Mark Wagie <mark at manjaro dot org>
# Contributor: Dan Johansen
# Contributor: Danct12 <danct12@disroot.org>

pkgname=squeekboard
pkgver=1.43.1
pkgrel=0
pkgdesc="An on-screen-keyboard input method for Wayland"
arch=('x86_64' 'armv7h' 'aarch64')
url="https://gitlab.gnome.org/World/Phosh/squeekboard"
license=('GPL-3.0-or-later')
depends=(
  'feedbackd'
  'gnome-desktop'
  'gtk3'
  'noto-fonts-emoji'
  'python-gobject'
  'wayland'
)
makedepends=(
  'cargo'
  'git'
  'glib2-devel'
  'libbsd'
  'meson'
  'wayland-protocols'
)
source=("git+https://gitlab.gnome.org/World/Phosh/squeekboard.git#tag=v$pkgver")
sha256sums=('97e44499f792b37dee7c225faa9e45eb8ed3e78e2a7203f17eaf8adc309d2194')

prepare() {
  cd "${pkgname}"
  local src
  for src in "${source[@]}"; do
    src="${src%%::*}"
    src="${src##*/}"
    [[ "${src}" = *.patch ]] || continue
    echo "Applying patch ${src}..."
    patch -Np1 < "../${src}"
  done

  export RUSTUP_TOOLCHAIN=stable
  cargo fetch --target "$(rustc -vV | sed -n 's/host: //p')"
}

build() {
  export RUSTUP_TOOLCHAIN=stable
  arch-meson "${pkgname}" build
  meson compile -C build
}

check() {
  export RUSTUP_TOOLCHAIN=stable
  meson test -C build --print-errorlogs
}

package() {
  meson install -C build --no-rebuild --destdir "${pkgdir}"
}
